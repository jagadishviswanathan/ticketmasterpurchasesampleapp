# Make sure to add your API key in AppDelegate.swift

Integration note:
* The version of the Presence SDK provided in the DemoApp Frameworks folder has been tested successfully with the Purchase SDK. 
* However, it is recommended that you use the latest version of the Presence SDK, which can be found here:
  - https://developer.ticketmaster.com/products-and-docs/sdks/presence-sdk/

Release Notes:
* v0.3.2a - Aug/31/2020
 - Added latest Presence SDK 1.28.0 (with Swift ABI Stability)
 - Refactored Purchase Demo App to work correctly with new Presence SDK class name
 - Verified Demo App works on Xcode 12 beta 6 (iOS 14)
 - No changes to Purchase SDK itself, so version number stays the same (0.3.2)
* v0.3.2 - Aug/11/2020
  - Added working framework stripping example to Purchase DemoApp
  - Added latest Presence SDK 1.27.0
  - Added latest TicketmasterFoundation SDK 0.2.2
* v0.3.1 - Jul/15/2020
  - Fixed issue where cart would not cancel countdown timer upon order completion
* v0.3.0 - Apr/28/2020
  - Added support for new Modern Accounts flow (via Presence SDK 1.24.0)
  - Added support for new Presence SDK asyncronous logout flow 
  - Migration from www1.ticketmaster.com to www.ticketmaster.com to improve EDP load times 
* v0.2.3 - Apr/6/2020
  - Fixed issue with Purchase Demo App referencing private Presence SDK v1.23.0 methods
  - Note there was no change to actual Purchase SDK itself (still v0.2.2)
* v0.2.2 - Mar/27/2020
  - Fixed Checkout 2.0 URLs not being detected properly
* v0.2.1 - Mar/24/2020
  - Added proper error messaging for nightly Maintenance Mode
  - Fixed blank webpage issue when dismissing error
* v0.2.0 - Mar/20/2020
  - PresenceSDKWrapper class now properly exposes Host vs. AccountManager logins
  - Improved documentation
* v0.1.4 - Nov/14/2019
  - Improved debug logging
* v.0.1.3 - Nov/12/2019
  - Added showShareToolbarButton to TMPurchaseWebsiteConfiguration
  - Fixed login cancel dismissing EDP
  - Fixed performance issue with navigation bar text
  - Fixed top crasher (rightToolbarItem crash)
* v.0.1.2 - Nov/11/2019
  - Fixed issues with ABI frameworks containing classes with same name as framework
* v.0.1.1 - Nov/8/2019
  - Removed deprecated methods and properties, added Swift stability ABI
* v0.1.0 - Oct/31/2019
  - Purchase SDK now support Ticketmaster SmartQueue for high-volume onsales
  - However this caused breaking changes to be made to TMPurchaseViewControllerOAuthDelegate, see sample app for details
  - TicketmasterFoundation.framework now required
* v0.0.15 - Oct/30/2019
  - Xcode 11.1, Swift 5.1 support, removed iOS 13 "stacked" headers
* v0.0.14 - Sep/17/2019
  - Added new TMPurchaseWebsiteConfiguration for website/clubsite configuration
  - Deprecated old methods of setting values on Purchase SDK
  - Sample app now shows full usage of Discovery SDK, Purchase SDK, and Presence SDK
* v0.0.13 - Jul/26/2019
  - Added debug logging to Purchase SDK
  - Refactored sample app to better demonstrate Purchase vs Presence SDK responsibilities
  - Added info, calendar, and share features
  - Added french language updates for Canadian events and users with their language settings set to French (Canada)
* v0.0.12 - Jul/25/2019
  - Fix for infinite loading spinner issue
* v0.0.11 - Jul/24/2019
  - Fixed crash when parsing empty Event details arrays
  - Refactored all JSON parsing to use standard methods
  - Fixed bitcode compilation issue when Archiving framework
  - Added new Universal target scheme
  - Added readme.md and create_universal_framework.sh to project explorer for convenience
  - Added "How to Build" section to this readme.md
  - Refactored example app to split out various delegate methods into seperate Swift extension files
  - Added more details to Presence SDK callbacks in example app
  - Made sure Presence SDK is always called on main thread in example app
* v0.0.10 - Jul/15/2019
  - Support for Swift 5
  - Tm1 Integration for PurchaseSDK Analytics
  - Improved Analytics callbacks and documentation
* v0.0.8 - Sep/11/2018
  - Fix where sometimes the clicking on Done button would not trigger callback 
* v0.0.7 - Jul/30/2018
  - Minor bug fixes
* v0.0.6 - Jul/18/2018
  - Fixed loading spinner bug
  - Exposed Swift interface to Obj-C
  - Support for Swift 4.1
