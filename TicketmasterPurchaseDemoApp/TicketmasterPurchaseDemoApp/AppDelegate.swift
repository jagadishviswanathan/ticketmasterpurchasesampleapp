//
//  AppDelegate.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Alex Scharch on 8/3/17.
//  Copyright © 2017 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterFoundation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private let apiKey = "4Rxidqi6tTzrNwTNhAIgGfhhhJ4nr7BI" // your Ticketmaster/PresenceSDK API key
    private let displayName: String? = nil // unique team name
    private let useNewAccountsManager: Bool = false // use new AccountsManager password reset flow (if you have switched over to it)
    
    // some example colors
    static let tmBlue = UIColor(red: 0, green: 156/255, blue: 222/255, alpha: 1.0) // Ticketmaster blue (default)
    static let lnRed = UIColor(red: 0.8828125, green: 0.09375, blue: 0.2109375, alpha: 1.0) // LiveNation red
    static let bucksGreen = UIColor(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1.0) // Bucks team brand color
    
    static let brandColor = bucksGreen // branding color

    static let brandNameKey = "bucks" // unique branding name
    
    static let discreteID = "password" // marketing code
    static let cameFromCode = "bucks" // marketing code

    static let eventID = "0900571CEDCD5417" // (Host/TAP or Discovery ID)
    static let artistID = "805969" // Milwaukee Bucks (TM Artist ID)
    static let venueID = "57843" // Fiserv Forum, Milwaukee, WI (TM Venue ID)
        
    private let discoveryBrowseMarket = MarketLocation.Wisconsin_Milwaukee() // used for Discovery SDK homepage browsing

    var window: UIWindow?
    
    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        // 1. configure Discovery SDK for searching/discovering events
        DiscoverySDKWrapper.configure(initialBrowseMarket: discoveryBrowseMarket,
                                      navBarColor: AppDelegate.brandColor)
        
        // 2. configure Purchase SDK for purchasing tickets for an event
        PurchaseSDKWrapper.configure(apiKey: apiKey,
                                     navBarColor: AppDelegate.brandColor,
                                     isDebugMode: true)

        // 3. configure Presence SDK for login functions and displaying purchased tickets
        PresenceSDKWrapper.configure(apiKey: apiKey, displayName: displayName,
                                     navBarColor: AppDelegate.brandColor,
                                     ticketColor: AppDelegate.brandColor,
                                     buttonColor: AppDelegate.brandColor,
                                     useNewAccountsManager: useNewAccountsManager)
        
        return true
    }
    
}
