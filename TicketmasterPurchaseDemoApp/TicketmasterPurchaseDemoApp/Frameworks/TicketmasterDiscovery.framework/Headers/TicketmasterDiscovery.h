//
//  TicketmasterDiscovery.h
//  TicketmasterDiscoveryApp
//
//  Created by Aliaksandr Sinkevich on 8/1/18.
//  Copyright © 2018 Ticketmaster. All rights reserved.
//

@import Foundation;

//! Project version number for TicketmasterDiscovery.
FOUNDATION_EXPORT double TicketmasterDiscoveryVersionNumber;

//! Project version string for TicketmasterDiscovery.
FOUNDATION_EXPORT const unsigned char TicketmasterDiscoveryVersionString[];
