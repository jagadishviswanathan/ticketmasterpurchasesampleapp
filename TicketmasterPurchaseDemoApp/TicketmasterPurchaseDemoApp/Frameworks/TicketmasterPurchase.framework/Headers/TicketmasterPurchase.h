//
//  TicketmasterPurchase.h
//  TicketmasterPurchase
//
//  Created by Alex Scharch on 8/3/17.
//  Copyright © 2017 Ticketmaster. All rights reserved.
//

@import Foundation;

//! Project version number for TicketmasterPurchase.
FOUNDATION_EXPORT double TicketmasterPurchaseVersionNumber;

//! Project version string for TicketmasterPurchase.
FOUNDATION_EXPORT const unsigned char TicketmasterPurchaseVersionString[];

// Xcode 11.2 Known Issues
//  If a module is built with BUILD_LIBRARIES_FOR_DISTRIBUTION and
//  contains a public type with the same name as the module itself,
//  clients will fail to import the module. (19481048) (FB5863238)
// Workaround: Rename either the type or the module to remove the conflict.
