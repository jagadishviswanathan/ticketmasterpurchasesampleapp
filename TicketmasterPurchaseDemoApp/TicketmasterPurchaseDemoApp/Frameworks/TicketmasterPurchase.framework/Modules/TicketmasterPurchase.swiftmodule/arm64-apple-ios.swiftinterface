// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.2.2 (swiftlang-1103.0.32.6 clang-1103.0.32.51)
// swift-module-flags: -target arm64-apple-ios11.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name TicketmasterPurchase
import Contacts
import CoreLocation
import Foundation
import SafariServices
import Swift
import TicketmasterFoundation
@_exported import TicketmasterPurchase
import UIKit
import WebKit
@objc public protocol TMPurchaseViewControllerUserNavigationDelegate : AnyObject {
  @objc(purchaseViewController:didBeginTicketSelectionForEvent:) optional func purchaseViewController(_ purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, didBeginTicketSelectionFor event: TicketmasterPurchase.TMEvent)
  @objc(purchaseViewController:didEndTicketSelectionForEvent:becauseReason:) optional func purchaseViewController(_ purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, didEndTicketSelectionFor event: TicketmasterPurchase.TMEvent, because reason: TicketmasterPurchase.TMEndTicketSelectionReason)
  @objc(purchaseViewController:didBeginCheckoutForEvent:) optional func purchaseViewController(_ purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, didBeginCheckoutFor event: TicketmasterPurchase.TMEvent)
  @objc(purchaseViewController:didEndCheckoutForEvent:becauseReason:) optional func purchaseViewController(_ purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, didEndCheckoutFor event: TicketmasterPurchase.TMEvent, because reason: TicketmasterPurchase.TMEndCheckoutReason)
  @objc(purchaseViewController:didMakePurchaseForEvent:withOrder:) optional func purchaseViewController(_ purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, didMakePurchaseFor event: TicketmasterPurchase.TMEvent, with order: TicketmasterPurchase.TMOrder)
  @objc(purchaseViewControllerDidEnd:) optional func purchaseViewControllerDidFinish(_ purchaseViewController: TicketmasterPurchase.TMPurchaseViewController)
}
@objc public enum TMEndTicketSelectionReason : Swift.Int {
  case userDismissedTicketSelection
  case ticketSelectionErrored
  case checkoutBegan
  case maintenanceMode
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
@objc public enum TMEndCheckoutReason : Swift.Int {
  case userDismissedCheckout
  case checkoutCartErrored
  case cartExpired
  case userCompletedPurchase
  public typealias RawValue = Swift.Int
  public init?(rawValue: Swift.Int)
  public var rawValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objc final public class TMPurchase : ObjectiveC.NSObject {
  @objc public static var apiKey: Swift.String? {
    @objc get
    @objc set
  }
  @objc public static var brandColor: UIKit.UIColor
  @objc(debugMode) public static var isDebugMode: Swift.Bool
  public static var useWWW1URL: Swift.Bool
  @objc deinit
  @objc override dynamic public init()
}
@objc final public class TMPurchaseWebsiteConfiguration : ObjectiveC.NSObject {
  @objc final public var eventID: Swift.String
  @objc final public var attractionID: Swift.String?
  @objc final public var brandName: Swift.String?
  @objc final public var showNFLBranding: Swift.Bool
  @objc final public var discreteID: Swift.String?
  @objc final public var cameFromCode: Swift.String?
  @objc final public var resaleSoftLanding: Swift.Bool
  @objc final public var showResaleMessage: Swift.Bool
  @objc final public var showShareToolbarButton: Swift.Bool
  @objc final public var additionalURLParameters: [Swift.String : Swift.String]
  @objc final public var internalKeys: [Swift.String : Swift.String]
  @objc public init(eventID: Swift.String)
  @objc deinit
  @objc override dynamic public init()
}
@_hasMissingDesignatedInitializers @objc final public class TMAttraction : ObjectiveC.NSObject {
  @objc final public let discoveryID: Swift.String
  @objc final public let hostID: Swift.String?
  @objc final public let name: Swift.String
  @objc override dynamic public init()
  @objc deinit
}
@objc final public class TMUserToken : ObjectiveC.NSObject {
  @objc final public let value: Swift.String
  @objc final public let expirationDate: Foundation.Date?
  @objc final public let hmacID: Swift.String?
  @objc final public let doNotSell: Swift.Bool
  @objc public init(value: Swift.String, expirationDate: Foundation.Date? = nil, hmacID: Swift.String? = nil, doNotSell: Swift.Bool = false)
  @objc deinit
  @objc override dynamic public init()
}
@objc public protocol TMPurchaseViewControllerSharingDelegate : AnyObject {
  @objc(getSharingMessageForPurchaseViewController:event:completion:) func getSharingMessage(for purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, event: TicketmasterPurchase.TMEvent, completion: @escaping (Swift.String) -> Swift.Void)
}
@_hasMissingDesignatedInitializers @objc final public class TMOrder : ObjectiveC.NSObject {
  @objc final public let identifier: Swift.String?
  @objc final public let isResale: Swift.Bool
  @objc final public let ticketQuantity: Swift.Int
  @objc final public let basePriceTotal: Foundation.NSDecimalNumber?
  @objc final public let grandTotal: Foundation.NSDecimalNumber?
  @objc final public let currencyCode: Swift.String?
  @objc deinit
  @objc override dynamic public init()
}
@_hasMissingDesignatedInitializers @objc final public class TMEvent : ObjectiveC.NSObject {
  @objc final public let discoveryID: Swift.String
  @objc final public let hostID: Swift.String?
  @objc final public let name: Swift.String
  @objc final public let date: Foundation.Date?
  @objc final public let attractions: [TicketmasterPurchase.TMAttraction]?
  @objc final public let venues: [TicketmasterPurchase.TMVenue]?
  @objc override dynamic public init()
  @objc deinit
}
@objc public protocol TMPurchaseViewControllerOAuthDelegate : AnyObject {
  @objc(fetchLoginTokenOrPresentUIForPurchaseViewController:success:failure:) func fetchLoginTokenOrPresentUI(for purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, success: @escaping (TicketmasterPurchase.TMUserToken) -> Swift.Void, failure: @escaping () -> Swift.Void)
  @objc(logoutThenPresentLoginUIForPurchaseViewController:success:failure:) func logoutThenPresentLoginUI(for purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, success: @escaping (TicketmasterPurchase.TMUserToken) -> Swift.Void, failure: @escaping () -> Swift.Void)
  @objc(fetchLoginTokenForPurchaseViewController:success:failure:) func fetchLoginToken(for purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, success: @escaping (TicketmasterPurchase.TMUserToken) -> Swift.Void, failure: @escaping () -> Swift.Void)
  @objc(updateLoginTokenForPurchaseViewController:success:failure:) func updateLoginToken(for purchaseViewController: TicketmasterPurchase.TMPurchaseViewController, success: @escaping (TicketmasterPurchase.TMUserToken) -> Swift.Void, failure: @escaping () -> Swift.Void)
}
@objc final public class TMPurchaseViewController : UIKit.UINavigationController {
  @objc weak final public var oAuthDelegate: TicketmasterPurchase.TMPurchaseViewControllerOAuthDelegate? {
    @objc get
    @objc set
  }
  @objc weak final public var sharingDelegate: TicketmasterPurchase.TMPurchaseViewControllerSharingDelegate? {
    @objc get
    @objc set
  }
  @objc weak final public var userNavigationDelegate: TicketmasterPurchase.TMPurchaseViewControllerUserNavigationDelegate? {
    @objc get
    @objc set
  }
  @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @objc override dynamic public init(nibName nibNameOrNil: Swift.String?, bundle nibBundleOrNil: Foundation.Bundle?)
  @objc public init(configuration: TicketmasterPurchase.TMPurchaseWebsiteConfiguration)
  @objc override final public var preferredStatusBarStyle: UIKit.UIStatusBarStyle {
    @objc get
  }
  @objc override final public func viewWillAppear(_ animated: Swift.Bool)
  @objc deinit
  @available(iOS 5.0, *)
  @objc override dynamic public init(navigationBarClass: Swift.AnyClass?, toolbarClass: Swift.AnyClass?)
  @objc override dynamic public init(rootViewController: UIKit.UIViewController)
}
@_hasMissingDesignatedInitializers @objc final public class TMVenue : ObjectiveC.NSObject {
  @objc final public let discoveryID: Swift.String
  @objc final public let hostID: Swift.String?
  @objc final public let name: Swift.String?
  @objc override dynamic public init()
  @objc deinit
}
extension TicketmasterPurchase.TMEndTicketSelectionReason : Swift.Equatable {}
extension TicketmasterPurchase.TMEndTicketSelectionReason : Swift.Hashable {}
extension TicketmasterPurchase.TMEndTicketSelectionReason : Swift.RawRepresentable {}
extension TicketmasterPurchase.TMEndCheckoutReason : Swift.Equatable {}
extension TicketmasterPurchase.TMEndCheckoutReason : Swift.Hashable {}
extension TicketmasterPurchase.TMEndCheckoutReason : Swift.RawRepresentable {}
