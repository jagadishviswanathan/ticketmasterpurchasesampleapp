//
//  MenuView.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Alex Scharch on 8/17/17.
//  Copyright © 2017 Ticketmaster. All rights reserved.
//

import UIKit

class MenuView: UIControl {

    enum SignInState {
        case signedIn(username: String)
        case signedOut
    }
    
    @IBOutlet weak var customEventIDTextField: UITextField?
    @IBOutlet weak var customAttractionIDTextField: UITextField?
    @IBOutlet weak var customVenueIDTextField: UITextField?
    
    @IBOutlet fileprivate weak var goButtonForEvent: UIButton?
    @IBOutlet fileprivate weak var goButtonForAttraction: UIButton?
    @IBOutlet fileprivate weak var goButtonForVenue: UIButton?

    @IBOutlet fileprivate weak var signInButton: UIButton?
    @IBOutlet fileprivate weak var signOutButton: UIButton?
    
    var signInState: SignInState = .signedOut {
        didSet {
            DispatchQueue.main.async {
                switch self.signInState {
                case .signedIn(let name):
                    self.signInButton?.isHidden = true
                    self.signOutButton?.setTitle(name, for: .normal)
                    self.signOutButton?.isHidden = false
                    
                case .signedOut:
                    self.signInButton?.isHidden = false
                    self.signOutButton?.setTitle("", for: .normal)
                    self.signOutButton?.isHidden = true
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //updateEntryFields()
        updateGoButtons()
        registerForTextFieldTextChangeNotifications()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
        
}


fileprivate extension MenuView {
    
    func registerForTextFieldTextChangeNotifications() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateGoButtons),
                                               name: UITextField.textDidChangeNotification,
                                               object: nil)
    }
    
    @objc func updateGoButtons() {
        goButtonForEvent?.isEnabled = customEventIDTextField?.hasNonemptyText == true
        goButtonForEvent?.alpha = customEventIDTextField?.hasNonemptyText == true ? 1.0 : 0.5
        
        goButtonForAttraction?.isEnabled = customAttractionIDTextField?.hasNonemptyText == true
        goButtonForAttraction?.alpha = customAttractionIDTextField?.hasNonemptyText == true ? 1.0 : 0.5
        
        goButtonForVenue?.isEnabled = customVenueIDTextField?.hasNonemptyText == true
        goButtonForVenue?.alpha = customVenueIDTextField?.hasNonemptyText == true ? 1.0 : 0.5
    }
    
    func updateEntryFields() {
        customEventIDTextField?.text = AppDelegate.eventID
        customAttractionIDTextField?.text = AppDelegate.artistID
        customVenueIDTextField?.text = AppDelegate.venueID
    }
}


extension UITextField {
    
    var hasNonemptyText: Bool {
        return text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false
    }
    
}
