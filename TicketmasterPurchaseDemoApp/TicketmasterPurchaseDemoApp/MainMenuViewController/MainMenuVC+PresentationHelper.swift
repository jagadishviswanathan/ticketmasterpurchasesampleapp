//
//  MainMenuVC+PresentationHelper.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 11/5/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterFoundation
import TicketmasterPurchase
import TicketmasterDiscovery

// MARK: - Purchase SDK presentation methods

extension MainMenuVC {
    
    func presentPurchaseViewControllerForCustomEvent() {
        guard let customEventID = menuView.customEventIDTextField?.text,
            menuView.customEventIDTextField?.hasNonemptyText == true else { return }
        
        presentPurchaseViewController(for: customEventID)
    }
    
    func presentPurchaseViewController(for eventID: String) {
        let purchaseViewController = buildPurchaseViewController(for: eventID)
        purchaseViewController.modalPresentationStyle = .fullScreen // iOS 13+ "stacked" header fix
        present(purchaseViewController, animated: true, completion: nil)
    }
    
    // create a new PurchaseSDK UINavigation controller with the given EventID
    private func buildPurchaseViewController(for eventID: String) -> UIViewController {
        // you may want to apply unique configuration to every EDP
        // but for this example app, we will apply a generic, shared configuration for all EDPs
        
        // create configuration with eventID (required)
        let configuration = TMPurchaseWebsiteConfiguration(eventID: eventID)
        
        // MARK: modify your custom web configuration here
        
        // additional supported properties (optional)
        configuration.brandName = AppDelegate.brandNameKey
        //configuration.attractionID = AppDelegate.artistID
        //configuration.showNFLBranding = true
        
        //configuration.discreteID = AppDelegate.discreteID
        //configuration.cameFromCode = AppDelegate.cameFromCode
        
        //configuration.resaleSoftLanding = true
        //configuration.showResaleMessage = true
        
        configuration.showShareToolbarButton = false
        
        // any custom parameters you also want to pass (will be URL-encoded for you)
        //configuration.additionalURLParameters = ["myCustomKey 1": "myCustomValue 1", "myCustomKey 2": "myCustomValue 2"]
        
        // create a TMPurchaseViewController with given configuration
        let purchaseViewController = PurchaseSDKWrapper.purchaseNavigationController(configuration: configuration)
        
        // setup MainMenuVC as a delegate for this TMPurchaseViewController
        purchaseViewController.oAuthDelegate = self // required for login with PresenceSDK
        purchaseViewController.userNavigationDelegate = self // optional, gives feedback about user actions
        purchaseViewController.sharingDelegate = self // optional, return a customized sharing message
        
        return purchaseViewController
    }

}



// MARK: - Presence SDK presentation methods

extension MainMenuVC {
    
    /// present Presence SDK Order Management page
    func presentPresenceOrdersViewController() {
        let presenceViewController = PresenceSDKWrapper.shared.presenceOrdersViewController()
        presenceViewController.modalPresentationStyle = .fullScreen // iOS 13+ "stacked" header fix
        present(presenceViewController, animated: true, completion: nil)
    }
    
    /// update UI with Presence SDK sign-in state
    func updateSignInUIFromPresence() {
        // for this example, we'll use a Ticketmaster .host login
        PresenceSDKWrapper.shared.fetchMemberInfo(backend: .host, success: { (presenceMember) in
            self.loginUpdatedWith(backend: .host, userName: presenceMember.email)
        }) {
            self.loginUpdatedWith(backend: .host, userName: nil)
        }
    }
}



// MARK: - Discovery SDK presentation methods

extension MainMenuVC {
    
    func presentDiscoveryViewControllerForCustomAttraction() {
        guard let customAttractionID = menuView.customAttractionIDTextField?.text,
            menuView.customAttractionIDTextField?.hasNonemptyText == true else { return }
        
        // market no needed for ADP page
        presentDiscoveryViewController(page: .attraction(identifier: customAttractionID))
    }
    
    func presentDiscoveryViewControllerForCustomVenue() {
        guard let customVenueID = menuView.customVenueIDTextField?.text,
            menuView.customVenueIDTextField?.hasNonemptyText == true else { return }
        
        // market not needed for VDP page
        presentDiscoveryViewController(page: .venue(identifier: customVenueID))
    }
    
    func presentDiscoveryViewController(page: DiscoveryPage) {
        let discoveryVC = buildDiscoveryViewController(page: page)
        discoveryVC.modalPresentationStyle = .fullScreen // iOS 13+ "stacked" header fix
        present(discoveryVC, animated: true, completion: nil)
        
        // keep a weak reference to the presented TMDiscoveryViewController (to present the Purchase SDK on top of)
        presentedDiscoveryViewController = discoveryVC
    }
    
    func presentDiscoveryToPurchaseViewController(eventID: String) {
        let purchaseViewController = buildPurchaseViewController(for: eventID)
        purchaseViewController.modalPresentationStyle = .fullScreen // iOS 13+ "stacked" header fix
        
        // make sure we present the PurchaseSDK on top of the presented DiscoverySDK
        presentedDiscoveryViewController?.present(purchaseViewController, animated: true, completion: nil)
    }
    
    private func buildDiscoveryViewController(page: DiscoveryPage) -> TMDiscoveryViewController {
        // create a TMDiscoveryViewController with given page and market
        let viewController =  DiscoverySDKWrapper.discoveryViewController(initialPage: page)
        
        // setup MainMenuVC as a delegate for this TMDiscoveryViewController
        viewController.navigationDelegate = self
        viewController.analyticsDelegate = self
        
        return viewController
    }
}
