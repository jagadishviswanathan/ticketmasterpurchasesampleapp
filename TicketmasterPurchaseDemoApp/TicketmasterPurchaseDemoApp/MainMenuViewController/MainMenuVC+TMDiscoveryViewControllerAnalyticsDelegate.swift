//
//  MainMenuVC+TMDiscoveryViewController+AnalyticsDelegate.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 9/3/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import TicketmasterFoundation
import TicketmasterDiscovery

/// Optional delegate that provides feedback of what user behavior is occuring inside the Discovery SDK's web pages
///
/// Note that this class is NOT required for Purchase SDK, Presence SDK integration
extension MainMenuVC: TMDiscoveryViewControllerAnalyticsDelegate {
    
    public func discoveryViewController(_ discoveryViewController: TMDiscoveryViewController, didFire pageView: UALPageView) {
        print("discoveryViewControllerDidFirePageView: \(pageView.name)")
    }
    
    public func discoveryViewController(_ discoveryViewController: TMDiscoveryViewController, didFire action: UALUserAction) {
        print("discoveryViewControllerDidFireAction: \(action.name)")
    }
    
}
