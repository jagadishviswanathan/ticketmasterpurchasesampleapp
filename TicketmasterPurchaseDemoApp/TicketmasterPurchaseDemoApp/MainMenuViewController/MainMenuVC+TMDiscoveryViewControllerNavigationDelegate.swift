//
//  MainDiscoVC+TMDiscoveryViewControllerDelegate.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Thomas Tang on 8/1/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterFoundation
import TicketmasterDiscovery

/// Required delegate that provides various functionality for the Discovery SDK
///
/// Note that this class is NOT required for Purchase SDK, Presence SDK integration
extension MainMenuVC: TMDiscoveryViewControllerNavigationDelegate {
        
    public func discoveryViewController(_ discoveryViewController: TMDiscoveryViewController, didNavigateToEventWith identifier: String) {
        // The user has entered an Event Detail Page (EDP) in the Discovery SDK
        print("discoveryViewController.DidNavigateToEventWithIdentitifer: \(identifier)")
        
        // EDPs are handled by the PurchaseSDK
        presentDiscoveryToPurchaseViewController(eventID: identifier)
    }
    
    public func discoveryViewControllerDidRequestSignIn(_ discoveryViewController: TMDiscoveryViewController,
                                                 completion: @escaping ((TMUser?) -> Void)) {
        // The user is trying to login in the Discovery SDL
        print("discoveryViewController.DidRequestSignIn")
        
        // Discovery SDK uses Ticketmaster .host accounts only
        PresenceSDKWrapper.shared.loginIfNeededTo(backend: .host, success: { (token) in
            // user is logged into Host, fetch member info
            PresenceSDKWrapper.shared.fetchMemberInfo(backend: .host, success: { (presenceMember) in
                // convert PresenceMember into TMUser
                let user = TMUser(email: presenceMember.email,
                                  firstName: presenceMember.firstName,
                                  lastName: presenceMember.lastName,
                                  memberID: presenceMember.id,
                                  hmacID: "", // not available from Presence SDK
                                  countryCode: presenceMember.country?.code ?? "840", // default to US (if PSDK does not provide country code)
                                  deviceID: UIDevice.current.identifierForVendor?.uuidString,
                                  accessToken: presenceMember.idToken)
                completion(user)
            }) {
                // Presence SDK: could not fetch user's member info
                completion(nil)
            }
        }) {
            // Presence SDK: user did not login
            completion(nil)
        }
        

    }
    
    public func discoveryViewControllerDidRequestCurrentLocation(_ discoveryViewController: TMDiscoveryViewController) {
        // DiscoverySDK want to fetch the user's current location (lat/long) to set a MarketLocation
        print("discoveryViewController.DidRequestCurrentLocation")
        
        // The lat/long is first converted to a MarketLocation and then called
        //   let newMarketLocationFromLatLong: MarketLocation = MarketLocation(fromLatLong: latLong)
        //   discoveryViewController.callWebViewChangeLocation(marketLocation: newMarketLocationFromLatLong)
        //
        // Note that MarketLocation(fromLatLong: CLLocation) is not yet supported
    }
    
    public func discoveryViewController(_ discoveryViewController: TMDiscoveryViewController, didChangeLocationTo location: MarketLocation) {
        // The user has changed browse markets in the DiscoverySDK
        print("discoveryViewController.DidChangeLocationTo(location:\(location.description())")
    }
    
    public func discoveryViewController(_ discoveryViewController: TMDiscoveryViewController, didShare pageTitle: String, and pageURL: URL, to activityType: UIActivity.ActivityType) {
        // The user has shared a page in the DiscoverySDK
        print("discoveryViewController.DidSharePageTitle: \(pageTitle) pageURL: \(pageURL.absoluteString)")
    }
    
    public func discoveryViewController(_ discoveryViewController: TMDiscoveryViewController, didFinishLoadingURL: URL?) {
        print("discoveryViewController.DidFinishLoadingURL: \(didFinishLoadingURL?.absoluteString ?? "<nil>")")
    }

}
