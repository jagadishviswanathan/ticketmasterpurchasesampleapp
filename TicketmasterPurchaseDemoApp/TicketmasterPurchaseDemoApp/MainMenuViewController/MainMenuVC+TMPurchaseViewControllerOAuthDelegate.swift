//
//  MainMenuVC+TMPurchaseViewControllerOAuthDelegate.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 7/24/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterPurchase

/// Required delegate that provides Ticketmaster "Host" login/oauth functionality for the Purchase SDK
extension MainMenuVC: TMPurchaseViewControllerOAuthDelegate {
    // Important Note! The Purchase SDK requires a user's Ticketmaster "Host" account in order to purchase tickets for a single event.
    // This is different from the user's Team "AccountManager" account used to buy Season tickets for a team's entire season of events.
    
    func fetchLoginTokenOrPresentUI(for purchaseViewController: TMPurchaseViewController, success: @escaping (TMUserToken) -> Void, failure: @escaping () -> Void) {
        print("PurchaseSDK requires a Host user login token to continue")
        
        // is the user logged into AccountManager, but not logged into host?
        if PresenceSDKWrapper.shared.isLoggedInto(backend: .accountManager) && !PresenceSDKWrapper.shared.isLoggedInto(backend: .host) {
            DispatchQueue.main.async {
                // From the user's point of view, he is already "logged in", yet we need to ask him to log into a different system to continue.
                // Present a UIAlert giving the user some context as to why he has to login again.
                // Feel free to customize this alert for your app...
                let alert = UIAlertController(title: "Additional Login Required",
                                              message: "In order to Purchase tickets to individual Ticketmaster events, you will need to login or create a Ticketmaster account.",
                                              preferredStyle: .alert)
                
                let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
                    // Task: fetch Host login token (if user logged in), else present Host login UI and return Host login token
                    PresenceSDKWrapper.shared.loginIfNeededTo(backend: .host, success: success, failure: failure)
                }
                alert.addAction(okAction)
                
                // there are various viewControllers presented on top of TMPurchaseViewController at this point, so just use the top one
                var topViewController: UIViewController = purchaseViewController
                while let child = topViewController.presentedViewController {
                    topViewController = child
                }
                
                topViewController.present(alert, animated: true, completion: nil)
            }
            
        } else {
            // no special UI alert needed in most cases, just show the Host login page
            
            // Task: fetch Host login token (if user logged in), else present Host login UI and return Host login token
            PresenceSDKWrapper.shared.loginIfNeededTo(backend: .host, success: success, failure: failure)
        }
    }
    
    func logoutThenPresentLoginUI(for purchaseViewController: TMPurchaseViewController, success: @escaping (TMUserToken) -> Void, failure: @escaping () -> Void) {
        print("PurchaseSDK requires the user to logout and log back into Host")
        
        // Task: logout, then present Host login UI and return Host login token
        PresenceSDKWrapper.shared.logoutThenLoginTo(backend: .host, success: success, failure: failure)
    }
    
    func fetchLoginToken(for purchaseViewController: TMPurchaseViewController, success: @escaping (TMUserToken) -> Void, failure: @escaping () -> Void) {
        print("PurchaseSDK has asked for a Host user login token (if available)")
        
        // Task: fetch Host login token (if user logged in), do NOT present any login UI
        PresenceSDKWrapper.shared.fetchRefreshToken(backend: .host, success: success, failure: failure)
    }
    
    func updateLoginToken(for purchaseViewController: TMPurchaseViewController, success: @escaping (TMUserToken) -> Void, failure: @escaping () -> Void) {
        print("PurchaseSDK has asked for a refreshed Host user login token (if available)")
        
        // Task: update/refresh Host login token (if user logged in), do NOT present any login UI
        PresenceSDKWrapper.shared.fetchRefreshToken(backend: .host, success: success, failure: failure)
    }
}
