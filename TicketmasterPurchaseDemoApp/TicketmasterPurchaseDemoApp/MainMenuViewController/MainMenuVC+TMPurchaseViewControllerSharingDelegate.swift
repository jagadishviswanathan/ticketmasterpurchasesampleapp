//
//  MainMenuVC+TMPurchaseViewControllerSharingDelegate.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Thomas Tang on 8/15/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterPurchase

/// Optional delegate that provides custom share messaging inside the Purchase SDK
extension MainMenuVC: TMPurchaseViewControllerSharingDelegate {
    
    /// Purchase SDK has requested a sharing message
    func getSharingMessage(for purchaseViewController: TMPurchaseViewController, event: TMEvent, completion: @escaping (String) -> Void) {
        print("purchaseViewController.getSharingMessage")
        
        // return a custom message to include as part of Sharing
        completion("Hey, I found these cool Bucks tickets for \(event.name)!")
    }
    
}

