//
//  MainMenuVC+TMPurchaseViewControllerUserNavigationDelegate.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 7/24/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterPurchase

/// Optional delegate that provides feedback of what user behavior is occuring inside the Purchase SDK
extension MainMenuVC: TMPurchaseViewControllerUserNavigationDelegate {
    
    /// user has selected some tickets on EDP
    func purchaseViewController(_ purchaseViewController: TMPurchaseViewController,
                                didBeginTicketSelectionFor event: TMEvent) {
        
        print("Ticket selection began for \(event.name) (\(event.discoveryID)) (\(event.hostID ?? ""))")
    }
    
    /// user left EDP for a `reason`
    func purchaseViewController(_ purchaseViewController: TMPurchaseViewController,
                                didEndTicketSelectionFor event: TMEvent,
                                because reason: TMEndTicketSelectionReason) {
        
        let humanReadableReason: String
        switch reason {
        case .userDismissedTicketSelection:
            humanReadableReason = "user dimissed ticket selection"
        case .ticketSelectionErrored:
            humanReadableReason = "ticket selection errored"
        case .checkoutBegan:
            humanReadableReason = "checkout began"
        case .maintenanceMode:
            humanReadableReason = "server in nightly maintenance mode"
        @unknown default:
            humanReadableReason = "new reason" // not part of Purchase SDK v0.1.1
        }
        
        print("Ticket selection ended for \(event.name) because \(humanReadableReason)")
    }
    
    /// user has entered Checkout Cart
    func purchaseViewController(_ purchaseViewController: TMPurchaseViewController,
                                didBeginCheckoutFor event: TMEvent) {
        
        print("Checkout began for \(event.name)")
    }
    
    /// user has left Checkout Cart for a `reason`
    func purchaseViewController(_ purchaseViewController: TMPurchaseViewController,
                                didEndCheckoutFor event: TMEvent,
                                because reason: TMEndCheckoutReason) {
        
        let humanReadableReason: String
        switch reason {
        case .userDismissedCheckout:
            humanReadableReason = "checkout was canceled by the user"
        case .cartExpired:
            humanReadableReason = "the cart expired due to a time limit"
        case .checkoutCartErrored:
            humanReadableReason = "checkout cart errored"
        case .userCompletedPurchase:
            humanReadableReason = "the user made a successful purchase"
        @unknown default:
            humanReadableReason = "new reason" // not part of Purchase SDK v0.1.1
        }
        
        print("Checkout ended for \(event.name) because \(humanReadableReason)")
    }
    
    /// user has purchased tickets in the Checkout Cart
    func purchaseViewController(_ purchaseViewController: TMPurchaseViewController,
                                didMakePurchaseFor event: TMEvent,
                                with order: TMOrder) {
        
        var logMessage = "Checkout completed for \(event.name)"
        
        if let orderID = order.identifier {
            logMessage += " with order ID \(orderID)"
        }
        
        print(logMessage)
    }
    
    /// TMPurchaseViewController has been dismissed
    func purchaseViewControllerDidFinish(_ purchaseViewController: TMPurchaseViewController) {
        print("Purchase view controller finished")
    }
    
}
