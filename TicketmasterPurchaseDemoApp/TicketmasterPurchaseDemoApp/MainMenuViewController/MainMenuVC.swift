//
//  MainMenuVC.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Alex Scharch on 8/3/17.
//  Copyright © 2017 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterFoundation
import TicketmasterPurchase
import TicketmasterDiscovery
import PresenceSDK

class MainMenuVC: UIViewController {
    
    // keep a reference to the presented TMDiscoveryViewController (if any)
    //   We are reusing a single TMDiscoveryViewController for all of the different use cases (which is simpler).
    //   But, if kept 4 copies in memory, we could switch between them quickly without having to reload the underlying page each time
    weak var presentedDiscoveryViewController: TMDiscoveryViewController?
    
    @IBOutlet weak var menuView: MenuView!

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "MainMenuVC"
        
        // since Presence SDK login may need to be presented at any time, set this up first
        // if the Presence SDK login state ever changes,
        //  this viewController will be notified via the LoginUpdateDelegate method
        PresenceSDKWrapper.shared.start(loginUpdateDelegate: self)
        
        // this will enable debug logging of the Purchase SDK
        // Do NOT set for a Production release!
        //MessageLogger.currentLogLevel = .debug
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // we have appeared, update login state
        updateSignInUIFromPresence()
    }
    
}



// MARK: - Handle IBActions from buttons

extension MainMenuVC {
    
    // MARK: Discovery Search
    
    @IBAction func goButtonTappedForEvent() {
        presentPurchaseViewControllerForCustomEvent()
    }
    
    @IBAction func goButtonTappedForAttraction() {
        presentDiscoveryViewControllerForCustomAttraction()
    }
    
    @IBAction func goButtonTappedForVenue() {
        presentDiscoveryViewControllerForCustomVenue()
    }
    
    // MARK: Discovery Pages
    
    @IBAction func discoveryHomePageButtonTapped() {
        presentDiscoveryViewController(page: .home)
    }
    
    @IBAction func discoverySearchPageButtonTapped() {
        presentDiscoveryViewController(page: .search)
    }
    
    @IBAction func discoveryAttractionPageButtonTapped() {
        presentDiscoveryViewController(page: .attraction(identifier: AppDelegate.artistID))
    }
    
    @IBAction func discoveryVenuePageButtonTapped() {
        presentDiscoveryViewController(page: .venue(identifier: AppDelegate.venueID))
    }
    
    // MARK: Purchase EDPs
    
    @IBAction func event1ButtonTapped() {
        // a test event
        presentPurchaseViewController(for: "vvG17ZfGnrgSNE") // Mobile Only Test Event
    }
    
    @IBAction func event2ButtonTapped() {
        // a sports event
        presentPurchaseViewController(for: "0B005836E9295745") // Orange Crush Demolition Derby (Aug 08, 2021)
    }
    
    @IBAction func event3ButtonTapped() {
        // a LiveNation music event
        presentPurchaseViewController(for: "0B005859B3FF4918") // Queen Nation - A Tribute to the Music of Queen (Aug 04, 2021)
    }
    
    @IBAction func event4ButtonTapped() {
        // your own test event
        presentPurchaseViewController(for: "09005749F6435737") // Bee Gees Tribute (Jan 22, 2021)
    }
    
    // MARK: Presence Orders/Tickets
    
    @IBAction func presenceViewTicketsPageButtonTapped() {
        presentPresenceOrdersViewController()
    }

    // MARK: Presence Login/Logout
    
    @IBAction func signInButtonTapped() {
        // force Presence SDK to logout and then try to log back in
        // for this example, we'll use a Ticketmaster .host login
        PresenceSDKWrapper.shared.logoutThenLoginTo(backend: .host, success: { (userToken) in
            // no additional actions needed, since the PresenceSDKLoginUpdateDelegate will handle UI updates
        }) {
            // some error, ignore
        }
    }
    
    @IBAction func signOutButtonTapped() {
        // logout of Presence SDK account
        // for this example, we'll logout of Ticketmaster .host
        PresenceSDKWrapper.shared.logoutOf(backend: .host, success: {
            // all is well
        }) { (error) in
            // typically an error only occurs if user tries to logout while offline
        }
    }
    
    // MARK: other UI events
    
    @IBAction func mainViewTapped() {
        menuView.customEventIDTextField?.resignFirstResponder()
        menuView.customAttractionIDTextField?.resignFirstResponder()
        menuView.customVenueIDTextField?.resignFirstResponder()
    }
}



// MARK: - PresenceSDKLoginUpdateDelegate methods

extension MainMenuVC: PresenceSDKLoginUpdateDelegate {
    func loginUpdatedWith(backend: PresenceLogin.BackendName, userName: String?) {
        guard let userName = userName else {
            menuView.signInState = .signedOut
            return
        }
        
        menuView.signInState = .signedIn(username: userName)
    }
}



// MARK: - UITextFieldDelegate methods

extension MainMenuVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard textField == menuView.customEventIDTextField else { return false }
        
        presentPurchaseViewControllerForCustomEvent()
        
        return true
    }
    
}
