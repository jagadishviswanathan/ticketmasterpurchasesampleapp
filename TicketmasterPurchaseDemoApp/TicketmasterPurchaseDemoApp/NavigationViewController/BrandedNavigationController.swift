//
//  BrandedNavigationController.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 11/6/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit

class BrandedNavigationController: UINavigationController {
    
    var backgroundColor: UIColor = AppDelegate.brandColor
    var textColor: UIColor = .white
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        
        brandNavigationBar()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        brandNavigationBar()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        brandNavigationBar()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func brandNavigationBar() {
        navigationBar.barTintColor = backgroundColor
        navigationBar.tintColor = textColor
        navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: textColor]

        // make color behind statusBar match navBar
        view.backgroundColor = backgroundColor
    }
}
