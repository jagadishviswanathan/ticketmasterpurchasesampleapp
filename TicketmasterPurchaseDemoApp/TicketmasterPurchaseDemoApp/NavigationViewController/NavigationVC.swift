//
//  NavigationVC.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 7/26/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit

/// This class tries to replicate all the different ways an app could present a new UIViewController.
/// It is mainly used for Ticketmaster's own internal testing purposes.
class NavigationVC: UIViewController {

    private var mainMenuVC: UIViewController? = nil
    
    private var newWindow: UIWindow? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if mainMenuVC == nil {
            // only load this once
            mainMenuVC = UIStoryboard(name: "MainMenu", bundle: nil).instantiateInitialViewController()

            guard let vc = mainMenuVC else {
                fatalError("Cannot load MainMenu initial view controller")
            }

            // MARK: some of the ways your code might present a new UIViewController (for Ticketmaster's testing purposes)
            addNavigation(controller: vc) // recommended
            //present(controller: vc) // also recommended
            
            //displayChild(controller: vc)
            //addSubview(controller: vc)
            //addRootViewControllerSubview(controller: vc)
            //replaceRootViewController(controller: vc)
            //newWindowRootView(controller: vc)
            //newWindowSubView(controller:vc)
        }
    }
    
}



// MARK: - Private helper methods

fileprivate extension NavigationVC {
    
    func present(controller: UIViewController) {
        controller.modalPresentationStyle = .fullScreen // iOS 13+ "stacked" header fix
        present(controller, animated: false, completion: nil)
    }
    
    func displayChild(controller: UIViewController) {
        addChild(controller)
        controller.view.frame = view.frame
        view.addSubview(controller.view)
        controller.didMove(toParent: self)
    }
    
    func addSubview(controller: UIViewController) {
        controller.view.frame = view.frame
        view.addSubview(controller.view)
    }
    
    func addNavigation(controller: UIViewController) {
        if let window = UIApplication.shared.delegate?.window {
            window?.rootViewController = BrandedNavigationController(rootViewController: controller)
        }
    }
    
    func addRootViewControllerSubview(controller: UIViewController) {
        if let window = UIApplication.shared.delegate?.window {
            window?.addSubview(controller.view)
        }
    }
    
    func replaceRootViewController(controller: UIViewController) {
        if let window = UIApplication.shared.delegate?.window {
            window?.rootViewController = controller
        }
    }
    
    func newWindowRootView(controller: UIViewController) {
        newWindow = UIWindow(frame: view.frame)
        newWindow?.rootViewController = controller
        newWindow?.makeKeyAndVisible()
    }
    
    func newWindowSubView(controller: UIViewController) {
        newWindow = UIWindow(frame: view.frame)
        newWindow?.addSubview(controller.view)
        newWindow?.makeKeyAndVisible()
    }

}
