//
//  DiscoverySDKWrapper.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Thomas Tang on 7/31/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterFoundation
import TicketmasterDiscovery

/// wrap all calls to Discovery SDK in an easy to read class
/// - Important: The Discovery SDK is a work in progress, so we currently discourage use in a production app
///
/// Note that this class is NOT required for Purchase SDK, Presence SDK integration
class DiscoverySDKWrapper {
    
    private static var initialBrowseMarket: MarketLocation? = nil
    
    /// Creates a `TicketmasterDiscovery` singleton used to present Home, Search, Artist, and Venue details
    ///
    /// - Parameters:
    ///   - initalBrowseMarket: optional initial Market to display upon browsing (New York, Los Angeles, etc)
    ///   - navBarColor: optional UIColor to apply to Discovery SDK's naviation bars
    static func configure(initialBrowseMarket: MarketLocation? = nil, navBarColor: UIColor? = nil) {
        print("- Configure DiscoverySDK")
        TMDiscovery.environment = .production
        if let color = navBarColor {
            TMDiscovery.navBarColor = color
        }
        self.initialBrowseMarket = initialBrowseMarket
    }
    
    /// Creates a TMDiscoveryViewController with the specified initial page and market
    ///
    /// - Parameters:
    ///   - initialPage: Discovery page type to create
    ///   - initialMarket: optional initial Market to display (New York, Los Angeles, etc)
    ///
    /// Note that the initialPage Swift enum may contain artistID or venueID values
    static func discoveryViewController(initialPage: DiscoveryPage) -> TMDiscoveryViewController {
        return TMDiscoveryViewController(initialPage: initialPage,
                                         marketLocation: DiscoverySDKWrapper.initialBrowseMarket ?? MarketLocation.UnitedStates_All(),
                                         user: nil,
                                         queryParameters: nil,
                                         canDismissSelf: true,
                                         allowJavaScriptToBringUpKeyboard: false)
    }
}
