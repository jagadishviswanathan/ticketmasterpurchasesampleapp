//
//  ObjectiveCExample.m
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 9/18/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

#import "ObjectiveCExample.h"

@import PresenceSDK;
@import TicketmasterDiscovery;
@import TicketmasterPurchase;

@interface ObjectiveCExample () <TMPurchaseViewControllerOAuthDelegate,
                                 TMPurchaseViewControllerUserNavigationDelegate>

@end

@implementation ObjectiveCExample

- (void)doIt {
    // TODO: add objective-c versions of the *SDKWrapper.swift files
    // for now, just call a few key methods to make sure objective-c has the proper visibility into the framework's *-Swift.h files
    
    
    // Configure Purchase SDK
    TMPurchase.apiKey = @"discoveryApiKey";
    TMPurchase.brandColor = [UIColor blueColor];

    // Configure Presence SDK
    PSDK* psdk = PSDK.getPresenceSDK;
    [psdk setConfigWithConsumerKey:@"discoveryApiKey" displayName:@"aName" useNewAccountsManager:NO];
    
    // Configure Discovery SDK
    TMDiscovery.navBarColor = [UIColor blueColor];

    
    // create a new Purchase SDK configuration
    TMPurchaseWebsiteConfiguration* configuration = [[TMPurchaseWebsiteConfiguration alloc] initWithEventID:@"12345"];
    configuration.brandName = @"bucks";
    configuration.attractionID = @"12345";
    configuration.showNFLBranding = YES;

    //configuration.discreteID = @"password";
    //configuration.cameFromCode = @"marketingCode";

    //configuration.resaleSoftLanding = YES;
    //configuration.showResaleMessage = YES;

    configuration.additionalURLParameters = @{@"myCustomKey 1": @"myCustomValue 1", @"myCustomKey 2": @"myCustomValue 2"};
    
    UINavigationController* controller = [[TMPurchaseViewController alloc] initWithConfiguration:configuration];
    controller.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:controller animated:NO completion:nil];
}



// MARK: - TMPurchaseViewControllerOAuthDelegate

- (void)logoutThenPresentLoginUIForPurchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController success:(void (^ _Nonnull)(TMUserToken * _Nonnull))success failure:(void (^ _Nonnull)(void))failure {
    // task: logout, then present login UI
    [PSDK.getPresenceSDK logOutWithCompletion:^(BOOL hostSuccess, NSError* hostError, BOOL accountManagerSuccess, NSError* accountManagerError) {
        if (hostSuccess) {
            [self loginToHostWithSuccess:success failure:failure]; // present login UI
        }
    }];
}

- (void)fetchLoginTokenOrPresentUIForPurchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController success:(void (^ _Nonnull)(TMUserToken * _Nonnull))success failure:(void (^ _Nonnull)(void))failure {
    // task: fetch login token (if user logged in), else present login UI
    if ([PSDK.getPresenceSDK isLoggedIntoHost]) {
        [self getRefreshHostTokenWithSuccess:success failure:failure];
    } else {
        [self loginToHostWithSuccess:success failure:failure]; // present login UI
    }
}

- (void)fetchLoginTokenForPurchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController success:(void (^ _Nonnull)(TMUserToken * _Nonnull))success failure:(void (^ _Nonnull)(void))failure {
    // task: fetch login token (if user logged in), do NOT present any login UI
    if ([PSDK.getPresenceSDK isLoggedIntoHost]) {
        [self getRefreshHostTokenWithSuccess:success failure:failure];
    } else {
        failure();
    }
}

- (void)updateLoginTokenForPurchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController success:(void (^ _Nonnull)(TMUserToken * _Nonnull))success failure:(void (^ _Nonnull)(void))failure {
    // task: update/refresh login token (if user logged in), do NOT present any login UI
    if ([PSDK.getPresenceSDK isLoggedIntoHost]) {
        [self getRefreshHostTokenWithSuccess:success failure:failure];
    } else {
        failure();
    }
}



// MARK: - TMPurchaseViewControllerUserNavigationDelegate

- (void)purchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController didBeginTicketSelectionForEvent:(TMEvent * _Nonnull)event {
    
}

- (void)purchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController didEndTicketSelectionForEvent:(TMEvent * _Nonnull)event becauseReason:(enum TMEndTicketSelectionReason)reason {
    
}

- (void)purchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController didBeginCheckoutForEvent:(TMEvent * _Nonnull)event {
    
}

- (void)purchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController didEndCheckoutForEvent:(TMEvent * _Nonnull)event becauseReason:(enum TMEndCheckoutReason)reason {
    
}

- (void)purchaseViewController:(TMPurchaseViewController * _Nonnull)purchaseViewController didMakePurchaseForEvent:(TMEvent * _Nonnull)event withOrder:(TMOrder * _Nonnull)order {
    
}

- (void)purchaseViewControllerDidEnd:(TMPurchaseViewController * _Nonnull)purchaseViewController {
    
}



// MARK: - Helper methods

- (void)loginToHostWithSuccess:(void (^ _Nonnull)(TMUserToken * _Nonnull))success failure:(void (^ _Nonnull)(void))failure {
    [PSDK.getPresenceSDK loginTo:0 completion:^(BOOL success) { }];
    // Presence SDK will call it's PresenceLoginDelegate.onLoginSuccessful() when ready
}

- (void)getRefreshHostTokenWithSuccess:(void (^ _Nonnull)(TMUserToken * _Nonnull))success failure:(void (^ _Nonnull)(void))failure {
    [PSDK.getPresenceSDK getAccessTokenWithBackendName:0 success:^(NSString* _Nonnull token) {
        // return valid token
        TMUserToken* userToken = [[TMUserToken alloc] initWithValue:token expirationDate:nil hmacID:nil doNotSell:false];
        success(userToken);
    } failure:^(NSError * _Nullable error, BOOL userCancelled) {
        failure();
    }];
}

@end
