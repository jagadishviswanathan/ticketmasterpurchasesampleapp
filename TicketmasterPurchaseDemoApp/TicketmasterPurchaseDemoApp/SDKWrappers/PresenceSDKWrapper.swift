//
//  PresenceSDKWrapper.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 7/26/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import PresenceSDK
import TicketmasterPurchase // for TMUserToken

/// a delegate to be notified if Presence SDK login state changes
@objc public protocol PresenceSDKLoginUpdateDelegate: class {
    /// user login state changed for a given backend
    ///
    /// - Parameters:
    ///   - backend: .host (Ticketmaster account) or .accountManager (Team account)
    ///   - userName: user's name or email address
    func loginUpdatedWith(backend: PresenceLogin.BackendName, userName: String?)
}

/// wrap all calls to Presence SDK in an easy to read class
@objc class PresenceSDKWrapper: NSObject {
    
    // MARK: Static Configuration

    static let shared = PresenceSDKWrapper()
    
    /// Creates a `PresenceSDK` singleton used to present Order and Tickets pages
    ///
    /// - Important: Do not initialize instances of `TMPresenceViewController` before
    ///   setting an API key via this method.
    ///
    /// - Parameters:
    ///   - apiKey: used for Ticketmaster's Presence SDK backend
    ///   - displayName: optional teamName, may be required for Ticketmaster's Presence SDK
    ///   - navBarColor: optional UIColor to apply to Presence SDK's naviation bars
    ///   - ticketColor: optional UIColor to apply to Presence SDK's ticket header
    ///   - buttonColor: optional UIColor to apply to Presence SDK's ticket buttons
    ///   - useNewAccountsManager: optional, use new AccountsManager password reset flow (if your team has switched over to it), otherwise use old flow
    static func configure(apiKey: String, displayName: String? = nil, navBarColor: UIColor? = nil, ticketColor: UIColor? = nil, buttonColor: UIColor? = nil, useNewAccountsManager: Bool = false) {
        if apiKey == "<Your API Key>" {
            print("Make sure to enter your own API Key into AppDelegate.swift")
        } else {
            print("- Configure PresenceSDK")
            
            PSDK.getPresenceSDK().setConfig(consumerKey: apiKey,
                                                   displayName: displayName,
                                                   useNewAccountsManager: useNewAccountsManager)
            
            // configure branding colors
            if let navBarColor = navBarColor {
                // navbar color used by TMPresenceOrdersViewController
                PresenceSDKWrapper.shared.navBarBackgroundColor = navBarColor
                
                // set colors inside Presence SDK
                let brandingColors = BrandingColors(navBarColor: navBarColor,
                                                    ticketColor: ticketColor ?? navBarColor,
                                                    buttonColor: buttonColor ?? navBarColor)
                PSDK.getPresenceSDK().setBrandingColors(brandingColors)
            }
        }
    }
    
    
    // private instance variables
    fileprivate var navBarTextColor: UIColor = .white
    fileprivate var navBarBackgroundColor: UIColor = UIColor(red: 0, green: 156/255, blue: 222/255, alpha: 1.0) // Ticketmaster blue
    
    fileprivate typealias LoginCompletion = (success: (TMUserToken) -> Void, failure: () -> Void)
    fileprivate var loginHostCompletion: LoginCompletion? = nil
    fileprivate var loginAccountManagerCompletion: LoginCompletion? = nil
    fileprivate var loginOtherCompletion: LoginCompletion? = nil

    fileprivate var loginUpdateDelegate: PresenceSDKLoginUpdateDelegate? = nil
    
    fileprivate var internalPresenceOrdersViewController: TMPresenceOrdersViewController? = nil
}



// MARK: - Public instance methods

extension PresenceSDKWrapper {
    
    // MARK: Startup Presence SDK
    
    /// Starts a PresenceSDKWrapper instance
    ///
    /// - Important: Call `PresenceSDKWrapper.configure(apiKey:)` has been called before this method
    ///
    /// - Parameters:
    ///   - loginUpdateDelegate: a delegate to be notified if Presence SDK login state changes
    func start(loginUpdateDelegate: PresenceSDKLoginUpdateDelegate?) {
        print("- Start PresenceSDK")
        DispatchQueue.main.async {
            self.loginUpdateDelegate = loginUpdateDelegate
            
            // create a new TMPresenceOrdersViewController (and internal PresenceSDKView)
            let presenceSDKView = self.presenceOrdersViewController().presenceView
            
            // this is an important trick:
            //  you must init PresenceSDKView BEFORE starting PresenceSDK
            //  and then provide the created PresenceSDKView into PresenceSDK.getPresenceSDK().start
            PSDK.getPresenceSDK().start(presenceSDKView: presenceSDKView,
                                        loginDelegate: self)
        }
    }
    
    // MARK: Manage Login State
    
    /// fetch login token (if user logged in), else present login UI and return login token
    ///
    /// - Important: Make sure `PresenceSDKWrapper.shared.start(loginUpdateDelegate:)` has been called before this method
    ///
    /// - Parameters:
    ///   - backend: .host (Ticketmaster account) or .accountManager (Team account)
    ///   - success: a TMUserToken either cached or from a successful login
    ///   - failure: something went wrong (user cancelled login, server issue, etc)
    func loginIfNeededTo(backend: PresenceLogin.BackendName, success: @escaping (TMUserToken) -> Void, failure: @escaping () -> Void) {
        if isLoggedInto(backend: backend) == false {
            // not logged in, present login ui
            loginTo(backend: backend, success: success, failure: failure)
        } else {
            // logged in, return token
            fetchRefreshToken(backend: backend, success: success, failure: failure)
        }
    }
    
    /// logout, then present login UI and return login token
    ///
    /// - Important: Make sure `PresenceSDKWrapper.shared.start(loginUpdateDelegate:)` has been called before this method
    ///
    /// - Parameters:
    ///   - backend: .host (Ticketmaster account) or .accountManager (Team account)
    ///   - success: a TMUserToken from a successful login
    ///   - failure: something went wrong (user cancelled login, server issue, etc)
    func logoutThenLoginTo(backend: PresenceLogin.BackendName, success: @escaping (TMUserToken) -> Void, failure: @escaping () -> Void) {
        // first, logout
        logoutOf(backend: backend, success: {
            // then, present login ui
            self.loginTo(backend: backend, success: success, failure: failure)
        }) { (error) in
            // typically an error only occurs if user tries to logout while offline
            // which means we won't be able to login either
            failure()
        }
    }
    
    /// logout of a particular backend account
    ///
    /// - Important: Make sure `PresenceSDKWrapper.shared.start(loginUpdateDelegate:)` has been called before this method
    ///
    /// - Parameters:
    ///   - backend: .host (Ticketmaster account) or .accountManager (Team account)
    func logoutOf(backend: PresenceLogin.BackendName, success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
        DispatchQueue.main.async {
            print("- PresenceSDK: Logout of \(backend.description)")
            
            // clear all captured blocks, then logout
            switch backend {
            case .host:
                self.loginHostCompletion = nil
                PSDK.getPresenceSDK().logOutHost(success: {
                    // all is well
                    success()
                }) { (error) in
                    // typically an error only occurs if user tries to logout while offline
                    failure(error as NSError?)
                }
            case .accountManager:
                self.loginAccountManagerCompletion = nil
                PSDK.getPresenceSDK().logOutTeam(success: {
                    // all is well
                    success()
                }) { (error) in
                    // typically an error only occurs if user tries to logout while offline
                    failure(error as NSError?)
                }
            @unknown default:
                self.loginOtherCompletion = nil
                // please implement whatever new .logOutX() is needed here
                failure(nil)
            }
        }
    }
    
    /// log out of all backend accounts (both .host and .accountManager)
    ///
    /// - Important: Make sure `PresenceSDKWrapper.shared.start(loginUpdateDelegate:)` has been called before this method
    ///
    func logoutOfAllAccounts(success: @escaping () -> Void, failure: @escaping (NSError?) -> Void) {
        DispatchQueue.main.async {
            print("- PresenceSDK: Logout of all Accounts (both .host and  .accountManager)")
            
            // clear all captured blocks
            self.loginHostCompletion = nil
            self.loginAccountManagerCompletion = nil
            self.loginOtherCompletion = nil
            
            // log out of both Host and Accounts Manager
            PSDK.getPresenceSDK().logOut { (hostSuccess, hostError, accountManagerSuccess, accountManagerError) in
                // typically an error only occurs if user tries to logout while offline
                if hostSuccess || accountManagerSuccess {
                    success()
                } else if let error = hostError {
                    failure(error as NSError?)
                } else if let error = accountManagerError {
                    failure(error as NSError?)
                } else {
                    failure(nil)
                }
            }
        }
    }
    
    // MARK: Refresh Login State
    
    /// fetch current access token (if any), if the token has expired, refresh it, then return it
    ///
    /// - Important: Make sure `PresenceSDKWrapper.shared.start(loginUpdateDelegate:)` has been called before this method
    ///
    /// - Parameters:
    ///   - backend: .host (Ticketmaster account) or .accountManager (Team account)
    ///   - success: a TMUserToken with a refreshed oauth token
    ///   - failure: something went wrong (user not logged in, refresh token expired, server issue, etc)
    func fetchRefreshToken(backend: PresenceLogin.BackendName, success: @escaping (TMUserToken) -> Void, failure: @escaping () -> Void) {
        DispatchQueue.main.async {
            if self.isLoggedInto(backend: backend) {
                // logged in, go get an updated OAuth token
                PSDK.getPresenceSDK().getAccessToken(backendName: .host, success: { (token) in
                    print("- PresenceSDK: Get/Refresh \(backend.description) Access Token: Success!")
                    success(TMUserToken(value: token))
                }, failure: { (error, cancel) in
                    if let error = error as NSError? {
                        print("- PresenceSDK: Get/Refresh \(backend.description) Access Token Error: \(error.localizedDescription)")
                    }
                    failure()
                })
            } else {
                print("- PresenceSDK: Get/Refresh \(backend.description) Access Token Error: Not Logged Into \(backend.description)")
                failure()
            }
        }
    }
    
    // MARK: Query Login State
    
    /// a simple synchronous request to determine if user if logged in
    ///
    /// - Important: Make sure `PresenceSDKWrapper.shared.start(loginUpdateDelegate:)` has been called before this method
    ///
    /// - Parameters:
    ///   - backend: .host (Ticketmaster account) or .accountManager (Team account)
    func isLoggedInto(backend: PresenceLogin.BackendName) -> Bool {
        switch backend {
        case .host:
            return PSDK.getPresenceSDK().isLoggedIntoHost()
        case .accountManager:
            return PSDK.getPresenceSDK().isLoggedIntoTeam()
        @unknown default:
            return false // please implement whatever new .isLoggedIntoX() is needed here
        }
    }
    
    /// fetch user account member info (if any)
    ///
    /// - Important: Make sure `PresenceSDKWrapper.shared.start(loginUpdateDelegate:)` has been called before this method
    ///
    /// - Parameters:
    ///   - backend: .host (Ticketmaster account) or .accountManager (Team account)
    ///   - success: a PresenceMember with a refreshed oauth token
    ///   - failure: something went wrong (user not logged in, token expired, server issue, etc)
    func fetchMemberInfo(backend: PresenceLogin.BackendName, success: @escaping (PresenceMember) -> Void, failure: @escaping () -> Void) {
        DispatchQueue.main.async {
            if self.isLoggedInto(backend: backend) {
                // logged in, go get member info
                print("- PresenceSDK: Fetching \(backend.description) Member Info...")
                PSDK.getPresenceSDK().getMemberInfo(backendName: backend) { (presenceMember, error) in
                    if let presenceMember = presenceMember {
                        print("- PresenceSDK: Fetch \(backend.description) Member Info: Success!")
                        success(presenceMember)
                    } else if let error = error as NSError? {
                        print("- PresenceSDK: Fetch \(backend.description) Member Info Error: \(error.localizedDescription)")
                        failure()
                    } else {
                        print("- PresenceSDK: Fetch \(backend.description) Member Info Error: <unknown>")
                        failure()
                    }
                }
            } else {
                print("- PresenceSDK: Fetch \(backend.description) Member Info Error: Not Logged Into \(backend.description)")
                failure()
            }
        }
    }
    
    // MARK: Present Orders and Tickets
    
    /// return a branded TMPresenceOrdersViewController for presentation
    ///
    /// - Important: Make sure `PresenceSDKWrapper.shared.start(loginUpdateDelegate:)` has been called before this method
    func presenceOrdersViewController() -> TMPresenceOrdersViewController {
        let output: TMPresenceOrdersViewController
        if let vc = internalPresenceOrdersViewController {
            // return existing
            output = vc
        } else {
            // create new
            output = TMPresenceOrdersViewController()
            internalPresenceOrdersViewController = output
        }
        
        // brand TMPresenceOrdersViewController navbar
        output.navBarBackgroundColor = navBarBackgroundColor
        output.navBarTextColor = navBarTextColor
        
        return output
    }
}




// MARK: - Private helper methods

fileprivate extension PresenceSDKWrapper {
    
    func loginTo(backend: PresenceLogin.BackendName, success: @escaping (TMUserToken) -> Void, failure: @escaping () -> Void) {
        DispatchQueue.main.async {
            // PresenceSDK uses delegate methods, but we want block behavior
            // so capture this completion block for later (both success and failure)
            switch backend {
            case .host:
                self.loginHostCompletion = LoginCompletion(success: success, failure: failure)
            case .accountManager:
                self.loginAccountManagerCompletion = LoginCompletion(success: success, failure: failure)
            @unknown default:
                self.loginOtherCompletion = LoginCompletion(success: success, failure: failure)
            }
            
            // then present login UI
            print("- PresenceSDK: Login to \(backend.description)...")
            PSDK.getPresenceSDK().login(to: backend)
            
            // Presence SDK will call it's PresenceLoginDelegate.onLoginSuccessful() when ready
            // which in turn will call the captured completion block (both success and failure)
        }
    }
    
    func completePresenceViewController(backend: PresenceLogin.BackendName, accessToken: String, presenceMember: PresenceMember?) {
        DispatchQueue.main.async {
            print("- PresenceSDK: \(backend.description) Login Complete")
            
            let token = TMUserToken(value: accessToken,
                                    doNotSell: presenceMember?.doNotSellFlag ?? false)
            // return the token to TMPurchaseViewControllerOAuthDelegate via the captured success block
            switch backend {
            case .host:
                self.loginHostCompletion?.success(token)
                self.loginHostCompletion = nil
            case .accountManager:
                self.loginAccountManagerCompletion?.success(token)
                self.loginAccountManagerCompletion = nil
            @unknown default:
                self.loginOtherCompletion?.success(token)
                self.loginOtherCompletion = nil
            }
            
            // let the delegate know there was a login update
            self.loginUpdateDelegate?.loginUpdatedWith(backend: backend, userName: presenceMember?.firstName ?? "<unknown user>")
        }
    }
    
    func dismissPresenceViewController(backend: PresenceLogin.BackendName) {
        DispatchQueue.main.async {
            print("- PresenceSDK: Dismiss \(backend.description) PresenceViewController")
            
            // dismiss Presence SDK viewController (if displayed)
            self.internalPresenceOrdersViewController?.dismiss(animated: true, completion: nil)
            
            // return to TMPurchaseViewControllerOAuthDelegate via the captured failure block
            switch backend {
            case .host:
                self.loginHostCompletion?.failure()
                self.loginHostCompletion = nil
            case .accountManager:
                self.loginAccountManagerCompletion?.failure()
                self.loginAccountManagerCompletion = nil
            @unknown default:
                self.loginOtherCompletion?.failure()
                self.loginOtherCompletion = nil
            }
            
            // let the delegate know there was a login update
            self.loginUpdateDelegate?.loginUpdatedWith(backend: backend, userName: nil)
        }
    }
}



// MARK: - Presence SDK delegate methods

extension PresenceSDKWrapper: PresenceLoginDelegate {
    
    // 1. Called when the LoginWindow is made visible to the user.
    func onLoginWindowDidDisplay(backendName: PresenceLogin.BackendName) {
        print("- PresenceSDK: \(backendName.description) onLoginWindowDidDisplay")
        
        // informational, nothing to really do here
    }
    
    // 2. User dismissed login window via the Cancel button
    func onLoginCancelled(backendName: PresenceLogin.BackendName) {
        print("- PresenceSDK: \(backendName.description) onLoginCancelled")
        
        dismissPresenceViewController(backend: backendName)
    }
    
    // 3. User granted app access/logged in
    func onLoginSuccessful(backendName: PresenceLogin.BackendName, accessToken: String) {
        print("- PresenceSDK: \(backendName.description) onLoginSuccessful")
        
        // Presence SDK logged in, now fetch memberInfo
        fetchMemberInfo(backend: backendName, success: { (presenceMember) in
            // loaded member info, return with accessToken
            self.completePresenceViewController(backend: backendName, accessToken: accessToken, presenceMember: presenceMember)
        }) {
            // could not load member info, just continue with accessToken
            self.completePresenceViewController(backend: backendName, accessToken: accessToken, presenceMember: nil)
        }
    }
    
    // 4. Something went wrong on the PresenceLogin backend
    func onLoginFailed(backendName: PresenceLogin.BackendName, error: NSError) {
        print("- PresenceSDK: \(backendName.description) onLoginFailed: \(error.localizedDescription)")
        
        dismissPresenceViewController(backend: backendName)
    }
    
    // 5a. User successfully logged-out of a PresenceLogin backend
    func onLogoutSuccessful(backendName: PresenceLogin.BackendName) {
        DispatchQueue.main.async {
            print("- PresenceSDK: onLogoutSuccessful (\(backendName.description))")
            
            // let the delegate know there was a login update
            self.loginUpdateDelegate?.loginUpdatedWith(backend: backendName, userName: nil)
        }
    }

    // 5b. User successfully logged-out of all PresenceLogin backends
    func onLogoutAllSuccessful() {
        DispatchQueue.main.async {
            print("- PresenceSDK: onLogoutAllSuccessful (both .host and .accountManager)")
            
            // let the delegate know there was a login update
            self.loginUpdateDelegate?.loginUpdatedWith(backend: .host, userName: nil)
        }
    }
    
}
