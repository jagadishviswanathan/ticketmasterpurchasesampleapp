//
//  PurchaseSDKWrapper.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 7/26/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit
import TicketmasterPurchase

/// wrap all calls to PurchaseSDK in an easy to read class
class PurchaseSDKWrapper {
    
    /// Creates a `TicketmasterPurchase` singleton used to present EDP and Checkout pages
    ///
    /// - Important: Do not initialize instances of `TMPurchaseViewController` before
    ///   setting an API key via this method.
    ///
    /// - Parameters:
    ///   - apiKey: used for Ticketmaster's Discovery API
    ///   - navBarColor: an optional UIColor to apply to Purchase SDK's naviation bars
    ///
    /// Instances of `TMPurchaseViewController` will use the value stored in  `TicketmasterPurchase.apiKey` when communicating with Ticketmaster's  Discovery API.
    /// To get an API key, visit the [Ticketmaster Developer Portal](https://developer.ticketmaster.com/).
    /// Your API key is listed as your application's Consumer Key under the My Apps section of your Ticketmaster Developer account.
    static func configure(apiKey: String, navBarColor: UIColor? = nil, isDebugMode: Bool = false) {
        if apiKey == "<Your API Key>" {
            fatalError("Make sure to enter your own API Key into AppDelegate.swift")
        } else {
            print("- Configure PurchaseSDK")
            TMPurchase.apiKey = apiKey
            if let color = navBarColor {
                TMPurchase.brandColor = color
            }
            TMPurchase.isDebugMode = isDebugMode
        }
    }

    /// Creates a TMPurchaseViewController with a unique website/clubsite configuration
    ///
    /// - Important: Do not call this method without calling PurchaseSDKWrapper.configure(apiKey:navBarColor) first
    ///
    /// - Parameters:
    ///   - configuration: a unique website/clubsite configuration
    static func purchaseNavigationController(configuration: TMPurchaseWebsiteConfiguration) -> TMPurchaseViewController {
        return TMPurchaseViewController(configuration: configuration)
    }

}
