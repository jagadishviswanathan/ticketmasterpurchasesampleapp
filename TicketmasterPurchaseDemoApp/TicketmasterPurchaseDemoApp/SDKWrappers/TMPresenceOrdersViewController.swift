//
//  TMPresenceOrdersViewController.swift
//  TicketmasterPurchaseDemoApp
//
//  Created by Jonathan Backer on 8/27/19.
//  Copyright © 2019 Ticketmaster. All rights reserved.
//

import UIKit
import PresenceSDK

/// The TMPresenceOrdersViewController wraps the PresenceSDKView(UIView) in a UIViewController for ease of use.
/// The PresenceSDKView is used to show the user's purchased Events and Tickets, but it is not required for login functionality.
///
/// Note: The DiscoverySDK and PurchaseSDK both already return a UIViewController, so this class is not needed for them.
class TMPresenceOrdersViewController: UIViewController {

    var navBarTextColor: UIColor = .white
    var navBarBackgroundColor: UIColor = .black
    
    private var navigationBar: UINavigationBar? = nil
    var presenceView: PresenceSDKView? = nil
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        setupPresenceView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupPresenceView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = "My Events"

        // do we need some kind of UI to dismiss this UIViewController?
        if let existingNavBar = navigationController?.navigationBar {
            // no, use existing bar
            navigationBar = existingNavBar
        } else {
            // yes, provide our own NavBar for this view
            setupNavigationBarForUIView()
        }
        
        // set branding for navigation bar
        brandNavigationBar()
        
        if let navigationBar = navigationBar, let presenceView = presenceView {
            view.addSubview(presenceView)
            view.addSubview(navigationBar)
            
            // build constraints to hold NavBar + UIView
            let viewArray = ["navBar": navigationBar , "presenceView": presenceView]
            var allConstraints: [NSLayoutConstraint] = []
            
            let vertConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-[navBar][presenceView]|", metrics: nil, views: viewArray)
            allConstraints += vertConstraints
            
            let horiz1Constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[presenceView]|", metrics: nil, views: viewArray)
            allConstraints += horiz1Constraints
            
            let horiz2Constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[navBar]|", metrics: nil, views: viewArray)
            allConstraints += horiz2Constraints

            NSLayoutConstraint.activate(allConstraints)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // present login if needed
        // for this example, we'll use a Ticketmaster .host login
        PresenceSDKWrapper.shared.loginIfNeededTo(backend: .host, success: { (userToken) in
            // no additional actions needed
        }) {
            // not logged in?
        }
    }
    
}



// MARK: - Private helper methods

extension TMPresenceOrdersViewController {
    
    @objc func doneButtonTapped() {
        dismiss(animated: true, completion: nil)
    }
    
    private func setupPresenceView() {
        // create a special view used to display PresenceSDK tickets
        presenceView = PresenceSDKView(frame: CGRect.zero)
        presenceView?.translatesAutoresizingMaskIntoConstraints = false
        presenceView?.backgroundColor = .white
    }
    
    private func setupNavigationBarForUIView() {
        // build a nav bar (it's noramlly made to work with a UIViewController, but we just want to use it with a UIView here)
        navigationBar = UINavigationBar(frame: CGRect.zero)
        navigationBar?.translatesAutoresizingMaskIntoConstraints = false
        navigationBar?.isTranslucent = false

        let navItem = UINavigationItem(title: title ?? "")
        navItem.rightBarButtonItem =  UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.done,
                                                      target: self,
                                                      action: #selector(doneButtonTapped))
        navigationBar?.setItems([navItem], animated: false)
    }
    
    private func brandNavigationBar() {
        navigationBar?.barTintColor = navBarBackgroundColor
        navigationBar?.tintColor = navBarTextColor
        navigationBar?.titleTextAttributes = [NSAttributedString.Key.foregroundColor: navBarTextColor]

        // make color behind statusBar match navBar
        view.backgroundColor = navigationBar?.barTintColor
    }

}
